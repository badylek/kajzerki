<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prognoza pogody Poznań</title>
    <link rel="stylesheet" href="styl4.css">
    <?php
   // $polacz = mysqli_connect("127.0.0.1","root","","prognoza");
    ?>
   <?php
    class Database {
       public $host;
       public $user;
       public $password;
       public $db_name;

       public $sql;
       
       public function __construct() {
           $this->host = "localhost";
           $this->user = "root";
           $this->password = "";
           $this->db_name = "prognoza";
       }

       public function polaczenie() {
         return mysqli_connect($this->host, $this->user, $this->password, $this->db_name);
       }

       public function zamknij() {
           return mysqli_close($this->polaczenie());
       }

       public function executeSql($query, $display, $table, $before, $after) {
        $this->sql = $query;

        $result = $this->polaczenie()->query($this->sql);

        if ($display == 1) {
            foreach(($result->fetch_all(MYSQLI_NUM)) as $row) {
                if($table == 1) {
                    echo "<tr>";
                    foreach($row as $r) {
                        echo "<td>";
                        echo $before.$r.$after;
                        echo "</td>";
                    }
                    echo "</tr>";
                } else {
                    foreach($row as $r) {
                    echo $before.$r.$after;
                    echo "<br/>";
                    }
                } 
            }
        } else {
            return $result;
        }
       }

    }

    $polacz = (new Database())->polaczenie();
    $zamknij = (new Database())->zamknij();
    
    //exit();
    ?>
</head>
<body>
    <section class= banerl>
    <p>wrzesien, 2020 r.</p>
    </section>
    
    <section class=baners>
    <h2>Prognoza dla Poznania</h2>
    
    </section>
    
    <section class=banerp>
    <img src="./logo.png" alt="prognoza">
    </section>
    
    <section class=lewy>

    <a href="kwerendy.txt" target="_blank">Kwerendy</a>

    </section>
    
    <section class=prawy>
    <img src="./obraz.jpg" alt="Polska, Poznań">
    </section>
    
    <section class=glowny>
    <?php
            //$polacz = mysqli_connect("127.0.0.1","root","","prognoza");
            //$sql = "SELECT * FROM pogoda WHERE miasta_id = 2 ORDER BY data_prognozy DESC";
            //$result = mysqli_query($polacz, $sql);
            //$id=0;
            ?>
            <h1>Zapyanie 1</h1>
        <table>
        <tr>
            <th>
            Lp.
            </th>
            <th>
            DATA
            </th>
            <th>
            NOC - TEMPERATURA
            </th>
            <th>
            DZIEŃ - TEMPERATURA
            </th>
            <th>
            OPADY [mm/h]
            </th>
            <th>
            CIŚNIENIE [hPa]
            </th>
        </tr>
            <?php
    $sql1 = (new Database())->executeSql("SELECT `id`, `miasta_id`, `data_prognozy`, `temperatura_noc`, `temperatura_dzien`, `opady`, `cisnienie` FROM `pogoda` ORDER BY `data_prognozy` DESC", 1, 1, "<b><u>", "</u></b>");
            ?>
        
        </table>
        <h1>Zapytanie 2</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT nazwa FROM `miasta`", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 3</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT opady from `pogoda` WHERE opady = '2'", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 4</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT temperatura_noc FROM `pogoda` WHERE data_prognozy >2020-09-14 AND data_prognozy <2020-09-30", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 5</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT opady from `pogoda` WHERE opady = '1'", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 6</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT cisnienie FROM `pogoda` WHERE cisnienie > 1000 AND cisnienie < 1018", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 7</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT AVG(temperatura_dzien)FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 8</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT MIN(temperatura_noc)FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 9</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT MAX(temperatura_noc)FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 10</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT AVG(opady)FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 11</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT COUNT(*) FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    ?>
    <h1>Zapytanie 12</h1>
        <?php
    $sql1 = (new Database())->executeSql("SELECT * FROM `pogoda` ORDER BY temperatura_dzien ASC", 1, 0, "<b><u>", "</u></b>");
    ?>
    </section>
    
    <section class=stopka>
    <p>Strone wykonał: 000000000</p>
    </section>
    
</body>
</html>