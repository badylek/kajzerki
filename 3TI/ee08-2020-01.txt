https://arkusze.pl/zawodowy/ee08-2020-styczen-egzamin-zawodowy-praktyczny.pdf


Do wykonania: 

Serwer instalujecie sobie na wirtualnej maszynie: 
https://www.virtualbox.org/wiki/Downloads
Do virtualboxa dołączacie sobie obraz windows servera oraz linuxa

https://www.microsoft.com/en-us/evalcenter/evaluate-windows-server-2012-r2
https://ubuntu.com/download/desktop

Po ściągnięciu obrazków dodajecie je w virtualboxie i konfigurujecie ustawiając w przypadku linuxa ok 1GB ramu, serwera ok 2GB)
W przypadku dysku dajcie ok 10GB na linuxa i serwer.

Wykonujecie te dwa zadania poprzedzając je screenami: 

Zadanie 6:
Skonfiguruj interfejsy sieciowe serwera w systemie Windows i stacji roboczej w systemie Linux:

 na serwerze skonfiguruj pierwszy interfejs sieciowy według zaleceń:
‒ nazwa połączenia: LAN1
‒ adres IP: 10.0.0.2/24
‒ brama domyślna: 10.0.0.1
‒ serwer DNS: 10.0.0.1
 na serwerze skonfiguruj drugi interfejs sieciowy według zaleceń:
‒ nazwa połączenia: LAN2
‒ adres IP: 192.168.0.x/24, gdzie x to numer stanowiska egzaminacyjnego
‒ brama domyślna: brak
‒ serwer DNS: brak
 na stacji roboczej skonfiguruj interfejs sieci przewodowej według zaleceń:
‒ adres IP: 90.90.90.2/30
 na stacji roboczej ustaw bramę domyślną na 90.90.90.1
 na stacji roboczej ustaw serwer DNS na 8.8.8.8
 na serwerze za pomocą polecenia ping sprawdź komunikację z drukarką, ruterem oraz stacją roboczą


Zadanie 7:
Skonfiguruj serwer z zainstalowanym systemem Windows Serwer:
 na serwerze dodaj rolę Serwer sieci Web, obsługujący protokół HTTP
 utwórz folder C:\www
 ustaw zabezpieczenia utworzonego folderu tylko dla:
 Administratorzy – Pełna kontrola
 Użytkownicy – Odczyt i wykonywanie
 w utworzonym folderze utwórz plik o nazwie test.html z zawartością:
<html>
<body>
<p>Strona testowa</p>
</body>
</html>
 utwórz nową witrynę sieci Web udostępniającą zawartość utworzonego folderu, tylko pod adresem IP
interfejsu LAN1 i portem 8080
 dla utworzonej witryny ustaw domyślny dokument na test.html
 sprawdź ze stacji roboczej, czy wyświetla się udostępniona witryna
===
DEADLINE 3/4/20
===